﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Shared.Models;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuestsController : ControllerBase
    {
        private readonly MainDbContext db;
        public QuestsController(MainDbContext db)
        {
            this.db = db;
        }

        [HttpGet]
        public async Task<ActionResult<List<Quest>>> GetQuests()
        {
            return await db.Quests.ToListAsync();
        }

        [HttpPost]
        public async Task<ActionResult<Quest>> CreateQuest(Quest quest)
        {
            db.Quests.Add(quest);
            await db.SaveChangesAsync();
            return quest;
        }

        [HttpPut]
        public async Task<ActionResult> UpdateQuest(Quest quest)
        {
            Quest existingQuest = db.Quests.FirstOrDefault(q => q.ID == quest.ID);
            if (existingQuest == null)
            {
                return NotFound();
            }

            db.Entry(existingQuest).CurrentValues.SetValues(quest);
            await db.SaveChangesAsync();

            return Ok();
        }

        [HttpDelete]
        public async Task<ActionResult> DeleteQuest(Quest quest)
        {
            Quest existingQuest = db.Quests.FirstOrDefault(q => q.ID == quest.ID);
            if (existingQuest == null)
            {
                return NotFound();
            }

            db.Remove(existingQuest);
            await db.SaveChangesAsync();

            return Ok();
        }
    }
}
