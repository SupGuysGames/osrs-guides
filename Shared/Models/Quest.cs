﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Shared.Models
{
    public class Quest
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required, StringLength(30, MinimumLength = 3)]
        public string Title { get; set; }
        [Required, Range(0, 1000)]
        public int InGameNumber { get; set; }
    }
}
